const HOURHAND = document.querySelector('#hour');
const MINUTEHAND = document.querySelector('#minute');
const SECONDHAND = document.querySelector('#second');

let date = new Date();

let hour = date.getHours();
let minute = date.getMinutes();
let second = date.getSeconds();

let circleInterval = 360 / 60;
let secPosition = second * circleInterval;
let minPosition = (minute * circleInterval) + (secPosition / 60);

let hrPosition = (hour * 360 / 12) + (minPosition / 12);
setInterval(() => {
  secPosition = secPosition + circleInterval;
  minPosition = minPosition + (1 / 60) * (circleInterval / 60);
  hrPosition = hrPosition + (3 / 360);
  HOURHAND.style.transform = 'rotate(' + hrPosition + 'deg)';
  MINUTEHAND.style.transform = 'rotate(' + minPosition + 'deg)';
  SECONDHAND.style.transform = 'rotate(' + secPosition + 'deg)';
}, 1000);
